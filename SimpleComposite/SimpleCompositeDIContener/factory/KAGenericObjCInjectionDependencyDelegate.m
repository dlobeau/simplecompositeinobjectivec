//
//  KAGenericObjCInjectionDependencyDelegate.m
//  SimpleComposite
//
//  Created by Didier Lobeau on 08/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "KAGenericObjCInjectionDependencyDelegate.h"
#import "KASeriazableObjectImp.h"
#import "KAFile.h"

@interface KAGenericObjCInjectionDependencyDelegate()
@property NSDictionary<NSString *,NSString *> *classNameAndAssociatedTagMapping;
@property NSArray<id<KAFile>> * injectionDependencyFileList;

@end

@implementation KAGenericObjCInjectionDependencyDelegate

@synthesize classNameAndAssociatedTagMapping = _classNameAndAssociatedTagMapping;


-(id) initWithInjectionFileList:(NSArray<id<KAFile>> *) injectionDependencyFileList
{
    self = [super init];
    self.injectionDependencyFileList = injectionDependencyFileList;
    
    return self;
}

-(NSDictionary<NSString *,NSString *> *)classNameAndAssociatedTagMapping
{
    if(_classNameAndAssociatedTagMapping == nil)
    {
        for(int i = 0; i< self.injectionDependencyFileList.count;i++)
        {
            id<KAFile> CurrentFile = [self.injectionDependencyFileList objectAtIndex:i];
            if(_classNameAndAssociatedTagMapping == nil)
            {
                _classNameAndAssociatedTagMapping = [[NSMutableDictionary alloc] init];
            }
            NSDictionary * DictinonaryFromFile = [self injectionFileContentWithFile:CurrentFile];
            [(NSMutableDictionary *)_classNameAndAssociatedTagMapping addEntriesFromDictionary:DictinonaryFromFile ];
        }
    }
    return _classNameAndAssociatedTagMapping;
}
-(void) setClassNameAndAssociatedTagMapping:(NSDictionary<NSString *,NSString *> *)classNameAndAssociatedTagMapping
{
    _classNameAndAssociatedTagMapping = classNameAndAssociatedTagMapping;
}

-(NSDictionary<NSString *,NSString *> *) injectionFileContentWithFile:(id<KAFile>)F
{
     NSDictionary * DictinonaryFromFile = [[NSDictionary alloc] initWithContentsOfFile:F.getFileCompleteName];
    
    return DictinonaryFromFile;
}


-(id) injectedObjectWithParameters:(NSDictionary *) Parameters
{
    NSString *GroupId = [Parameters objectForKey:[KASeriazableObjectImp getGroupIdTag]];
    NSAssert(GroupId !=nil , @"Group ID tag must be defined, check %@", Parameters);
       
    NSString *ClassName =[self.classNameAndAssociatedTagMapping objectForKey:GroupId];
    NSAssert(ClassName !=nil , @"Tag: \"%@\" is not assigned to any known class",GroupId);

    
    Class myClass = NSClassFromString(ClassName);
       
    NSAssert(myClass != nil, @"class %@ is not defined in project ",ClassName);
       
    id ReturnValue = [[myClass alloc]  initWithDictionary:Parameters];
    
    return ReturnValue;
}
@end
