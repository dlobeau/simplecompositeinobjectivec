//
//  KASerializableObjectToFile.h
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KASeriazableObject.h"

@interface KASerializableObjectToFile : NSObject

-(id) initWithSerializableObject:(id<KASeriazableObject>) SerializableObject;

-(void) serializeToFile:(id<KAFile>) File WithError:(NSError *) Error;


@end
