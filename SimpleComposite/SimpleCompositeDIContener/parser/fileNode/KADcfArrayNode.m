//
//  BVArrayNode.m
//  Skwipy
//
//  Created by Didier Lobeau on 04/02/2016.
//  Copyright (c) 2016 FollowTheDancer. All rights reserved.
//

#import "KADcfArrayNode.h"
#import "KADcfNodeFactory.h"

@implementation KADcfArrayNode


-(id) initWithArray:(NSArray *) array  WithNodeName:(NSString *)NodeName
{
    KADcfArrayNode *ReturnValue = [super init];
    ReturnValue.array = [array copy];
    ReturnValue.nodeName = [NodeName copy];
    ReturnValue.dictionary = nil;
    return ReturnValue;
}

-(NSArray *) getNodeChildList
{
    NSMutableArray * ReturnValue = nil;
    
    if( self.array != nil)
    {
        for( int i = 0; i < [self.array count]; i++)
        {
            id child = [self.array objectAtIndex:i];
            NSNumber *CountObject = [[NSNumber alloc]initWithInt:i];
            NSString *ChildKey = [@"item " stringByAppendingString:[CountObject stringValue]];
        
            if( ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            
            KADcfNodeFactory *Factory = [[KADcfNodeFactory alloc] init];
            id<KADcfNode> CurrentNodeChild = [Factory createParserNodeFromObject:child WithNodeName:ChildKey ];
            CurrentNodeChild.factoryDelegate =self.factoryDelegate;
            [ReturnValue addObject:CurrentNodeChild];
        }
        
    }
    return ReturnValue;
}

@end
