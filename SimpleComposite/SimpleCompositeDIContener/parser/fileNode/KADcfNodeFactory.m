//
//  KADcfNodeFactory.m
//  kakebo
//
//  Created by Didier Lobeau on 14/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADcfNodeFactory.h"
#import "KADcfDictionnaryNode.h"
#import "KADcfFileNode.h"
#import "KADcfArrayNode.h"

@implementation KADcfNodeFactory

-(id<KADcfNode>) createParserNodeFromObject:(id)Object WithNodeName:(NSString *)NodeName
{
    id<KADcfNode> ReturnValue = nil;
    
    if( [Object isKindOfClass:[NSDictionary class]] )
    {
        NSDictionary *Dico = Object;
        
        NSString *GroupType = [Dico objectForKey:@"group_id"];
        if([GroupType isEqual:@"File"])
        {
            ReturnValue = [[KADcfFileNode alloc] initWithDictionary:(NSDictionary *)Object WithNodeName:NodeName];
        }
        else
        {
            ReturnValue = [[KADcfDictionnaryNode alloc] initWithDictionary:(NSDictionary *)Object WithNodeName:NodeName];
        }
    }
    else if( [Object isKindOfClass:[NSArray class]] )
    {
        ReturnValue = [[KADcfArrayNode alloc] initWithArray:(NSArray *)Object WithNodeName:NodeName];
    }
    return ReturnValue;
}


@end