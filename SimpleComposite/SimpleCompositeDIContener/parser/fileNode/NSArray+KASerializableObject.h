//
//  NSArray+KASerializableObject.h
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(KASerializableObject)

-(NSString *) toString;

@end
