//
//  NSString.h
//  kakebo
//
//  Created by Didier Lobeau on 02/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface NSString (KASeriazableObject)

-(NSString *) fileNameFromPath;
-(NSArray<NSString *>*) nodeIDList;
+(NSString *) pathSeparator;

-(BOOL) isObjectFamilyReference;
-(NSString *) extractObjectFamilyMarkers;
-(NSDictionary<NSString *,NSString *> *) splitWithPathAndFamillyName;

+(NSString *) pathTag;
+(NSString *) objectFamilyNameTag;

-(NSString *) nextPathBranch;


-(NSString *) lastElementFromPath;
-(NSString *) firstElementFromPath;
-(NSNumber *) toNumber;




-(BOOL) isPathRoot;
@end
