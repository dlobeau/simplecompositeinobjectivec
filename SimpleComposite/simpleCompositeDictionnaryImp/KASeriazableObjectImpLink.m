//
//  KASeriazableObjectImpLink.m
//  kakebo
//
//  Created by Didier Lobeau on 23/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KASeriazableObjectImpLink.h"
#import "KASeriazableObjectTableImp.h"
#import "KABasicFile.h"
#import "NSString+KASeriazableObject.h"
#import "KASeriazableObject.h"
#import "KASerializeObjectFactory.h"
@protocol KAIterator;

@interface KASeriazableObjectImpLink()

    
    @property BOOL isCreatingSource;

@end

@implementation KASeriazableObjectImpLink

+(NSString *) getTypeTag
{
    return @"Link";
}



-(void) fetchSource
{
    if(self.sourceObject == nil)
    {
        @synchronized (self) {
            NSAssert(!self.isCreatingSource, @"Source is already under generation, entering infinite loop in link:%@ and path: %@",[super path], [super labelIdentifier]);
            
            self.isCreatingSource =  YES;
            NSString *Path = super.label;
            
            self.sourceObject = [self getChildwithTypeId:Path];
            
            while((self.sourceObject !=nil) &&
                  [self.sourceObject conformsToProtocol:@protocol(KASerializeObjectLink)])
            {
                self.sourceObject = [(id<KASerializeObjectLink>)self.sourceObject source];
            }
            
            self.isCreatingSource =  NO;
        }
       
    }
}




-(id<KASeriazableObject>) source
{
    [self fetchSource];
    
    return self.sourceObject;
}
-(void) setSource:(id<KASeriazableObject>) Source
{
    self.sourceObject = Source;
}





-(NSDictionary *) toDictionnary
{
    NSMutableDictionary *ReturnValue = nil;
    if(!self.sourceObject.isDeleted)
    {
       ReturnValue = [[NSMutableDictionary alloc] init];
      
        if(self.sourceObject != nil)
        {
            NSString *Path = [self.sourceObject refreshedPath];
            
            self.label = Path;
            self.labelIdentifier = Path;
        }
        
        [ReturnValue setObject:super.ID  forKey:[KASeriazableObjectImp getTypeIdTag]];
        [ReturnValue setObject:super.label  forKey:[KASeriazableObjectImp getLabelTag]];
        [ReturnValue setObject:super.objectFamilyName  forKey:[KASeriazableObjectImp getGroupIdTag]];
        [ReturnValue setObject:super.labelIdentifier  forKey:[KASeriazableObjectImp getLabeLIdentifierTag]];
    }
    return ReturnValue;
}

-(NSString *) label
{
    return [self.source label];
}



-(NSString *) labelIdentifier
{
    return [self.source labelIdentifier];
}


-(NSString *) objectFamilyName
{
    return [self.source objectFamilyName];
}

-(KASeriazableObjectImpLink *) cloneObject
{
    KASeriazableObjectImpLink * ReturnValue = nil;
    
    
    ReturnValue  = (KASeriazableObjectImpLink *)[self.factoryDelegate createAttributeFromLabel:[super.label copy] WithID:[super.labelIdentifier copy] WithGRoupId:[super.objectFamilyName copy] WithTypeId:[super.ID copy] ];
    
   ReturnValue.sourceObject = self.sourceObject;
    
    return ReturnValue;
}

-(id<KAIterator>) getIterator
{
    return [self.source  getIterator];
}

-(id<KAIterator>) getIteratorWithDynamicChildsWithID:(NSString *)ID
{
    return [self.source  getIteratorWithDynamicChildsWithID:ID];
}


- (NSComparisonResult ) compareAttribute:(id<KASerializeObjectLink> ) AttributeToBeCompared
{
    NSComparisonResult ReturnValue = -5;
    if( AttributeToBeCompared != nil)
    {
        if( [AttributeToBeCompared conformsToProtocol:@protocol(KASerializeObjectLink)])
        {
            if([self.sourcePath isEqual:AttributeToBeCompared.sourcePath])
            {
                ReturnValue = NSOrderedSame;
            }
               
        }
        else
        {
            ReturnValue = NSOrderedAscending;
        }
        
    }
    return ReturnValue;
}

-(NSString *) sourcePath
{
    return super.labelIdentifier;
}

-(id<KASeriazableObject>) serializableObject
{
    return self;
}



-(NSArray<id<KASeriazableObject>> *) childs
{
    return [self.source childs];
}

@end
