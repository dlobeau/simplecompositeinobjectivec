//
//  KATableFifoChildsManagement.h
//  kakebo
//
//  Created by Didier Lobeau on 10/04/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KATableChildsManagementDelegate.h"
#import "KAIterator.h"
NS_ASSUME_NONNULL_BEGIN

@protocol KASeriazableObjectTable;
@protocol KASeriazableObject;


@interface KATableFifoChildsManagement : NSObject<KATableChildsManagementDelegate>

-(id) initWithSender:(id<KASeriazableObjectTable>) Sender;

@end

@interface KAAttributeContenerIterator:NSObject <KAIterator>
-(NSInteger) index;
-(NSInteger) size;
-(id) initWithContener:(NSArray *) Contener;
-(id) initWithContener:(NSArray *) Contener WithAdditionalData:(NSArray<id<KASeriazableObject>> * ) List;

@end

NS_ASSUME_NONNULL_END
